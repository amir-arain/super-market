Installation

Atleast php 7.2.5 is required

1. clone project with "https://gitlab.com/amir-arain/super-market.git"

2. cd to project directory

3. run command "composer install"

4. copy .env.example to .env and set app url ( if required )

5. create a database and set its name and oter details (if required ) in .env

6. php artisan key:generate

7. php artisan migrate:fresh --seed

8. php artisan passport:install

9. php artisan storage:link

10. php artisan serve   or run from vhost ( if created )



2 default users has been created with seed

"admin@admin.com"   "password" which is admin
"user@user.com"   "password" which is user


few products with faker has been seeded into db

