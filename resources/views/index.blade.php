<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="undefined" crossorigin="anonymous">
    <style>
        .loading_wrap {
          width: 100%;
          height: 100%;
          overflow: hidden;
          background: rgba(250, 250, 250, 0.45);
          display: block;
          position: fixed;
          z-index: 9999;
        }

        .loader_logo {
          height: 80px;
          width: 80px;
          position: absolute;
          left: calc(50% - 50px);
          top: 38%;
        }

        .loader_logo img {
          height: auto;
          width: 100%;
        }

        .loading {

          border: 3px solid rgba(102, 51, 153, 0.45);
          position: absolute;
          left: calc(50% - 35px);
          top: 50%;
          width: 55px;
          height: 55px;
          border-radius: 50%;
          border-top-color: #663399;
          animation: loader 1s ease-in-out infinite;
          -webkit-animation: loader 1s ease-in-out infinite;
        }

        @keyframes  loader {
          to {
            -webkit-transform: rotate(360deg);
          }
        }

        @-webkit-keyframes loader {
          to {
            -webkit-transform: rotate(360deg);
          }
        }

      </style>

</head>

<body>
    <div class="loading_wrap" id="loading_wrap" style="display:none;">
        <div class="loading"></div>
      </div>
    <div id="app">
    </div>
</body>

</html>
