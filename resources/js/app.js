require("./bootstrap");
window.Vue = require("vue");
window.moment = require('moment');
import Vue from 'vue';
import App from "./views/App";
import router from "./router";
require('./components');
import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';
Vue.use(VueToast);
import BootstrapVue from "bootstrap-vue";
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
Vue.use(BootstrapVue);
import store from './store.js';


Vue.prototype.$myLoader = function(action) {
    var myLoader = document.getElementById("loading_wrap");

    if (myLoader) {
        if (action == 'hide') {
            myLoader.style.display = 'none';
        } else {
            myLoader.style.display = 'block';
        }
    }
};
Vue.prototype.moment = moment


new Vue({
    router: router,
    store,
    render: h => h(App)
}).$mount("#app");
