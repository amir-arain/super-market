import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

import App from "./views/App";
import Home from "./views/Home";
import Login from "./views/Login";
import Register from "./views/Register";
import SingleProduct from "./views/SingleProduct";
import Cart from "./views/Cart";
import Confirmation from "./views/Confirmation";
import UserDashBoard from "./views/UserDashBoard";
import Admin from "./views/Admin";

const router = new VueRouter({
    mode: "history",
    base: '',
    routes: [
        {
            path: "/",
            name: "home",
            component: Home,
        },
        {
            path: "/login",
            name: "login",
            component: Login,
        },
        {
            path: "/register",
            name: "register",
            component: Register,
        },
        {
            path: "/product/:slug",
            name: "single-products",
            component: SingleProduct,
        },
        {
            path: "/confirmation",
            name: "confirmation",
            component: Confirmation,
        },
        {
            path: "/cart",
            name: "cart",
            component: Cart,
        },
        {
            path: "/dashboard",
            name: "userdashboard",
            component: UserDashBoard,
            meta: {
                requiresAuth: true,
                is_user: true,
            },
        },
        {
            path: "/admin/:page",
            name: "admin-pages",
            component: Admin,
            meta: {
                requiresAuth: true,
                is_admin: true,
            },
        },
        {
            path: "/admin",
            name: "admin",
            component: Admin,
            meta: {
                requiresAuth: true,
                is_admin: true,
            },
        },
    ],
    app,
});

router.beforeEach((to, from, next) => {
    if (to.matched.some((record) => record.meta.requiresAuth)) {
        if (localStorage.getItem("store.token") == null) {
            next({
                path: "/login",
                params: { nextUrl: to.fullPath },
            });
        } else {
            let user = JSON.parse(localStorage.getItem("store.user"));
            if (to.matched.some((record) => record.meta.is_admin)) {
                if (user.is_admin == 1) {
                    next();
                } else {
                    next({ name: "userdashboard" });
                }
            } else if (to.matched.some((record) => record.meta.is_user)) {
                if (user.is_admin == 0) {
                    next();
                } else {
                    next({ name: "admin" });
                }
            }
            next();
        }
    } else {
        next();
    }
});
export default router;
