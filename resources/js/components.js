import Vue from 'vue';
import App from "./views/App";
import Home from "./views/Home";
import Login from "./views/Login";
import Register from "./views/Register";
import SingleProduct from "./views/SingleProduct";
import Cart from "./views/Cart";
import Confirmation from "./views/Confirmation";
import UserDashBoard from "./views/UserDashBoard";
import Admin from "./views/Admin";

Vue.component('app', App.default);
Vue.component('Home', Home.default);
Vue.component('Login', Login.default);
Vue.component('Register', Register.default);
Vue.component('single-product', SingleProduct.default);
Vue.component('Cart', Cart.default);
Vue.component('Confirmation', Confirmation.default);
Vue.component('Userdashboard', UserDashBoard.default);
Vue.component('Admin', Admin.default);
