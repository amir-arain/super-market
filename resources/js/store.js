import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from "vuex-persistedstate";
import _ from "lodash";
import functions from './mixin.js'

Vue.use(Vuex);

export default new Vuex.Store({
    plugins: [createPersistedState()],
    state: {
        cart: [],
        cartCounter:0
    },
    getters: {
        cart (state) {
            return state.cart;
        },
        cartCounter (state) {
            return state.cartCounter;
        },
    },

    mutations: {
        addToCart(state , data){
            state.cart = [...data]
        },
        cartCounter(state , data){
            state.cartCounter = data
        },
    },
    actions: {
        addToCartAction({commit, getters}, payload){
            var my_cart = getters.cart
            var cartCounter = getters.cartCounter
            let {product, notify, loading } = payload;
            if (Object.keys(my_cart).length) {
                var hasIncart = null;
                let index = _.findIndex(my_cart, (e) => {
                    if(e.id == product.id) {
                        hasIncart = true;
                        return e.id == product.id
                    }
                }, 0);
                if(hasIncart) {
                    my_cart[index].count = parseInt(my_cart[index].count) + 1;
                    cartCounter = parseInt(cartCounter) + 1;
                } else {
                    product.count = 1;
                    cartCounter = parseInt(cartCounter) + 1;
                    my_cart.push(product);
                }
                commit('addToCart', my_cart);
                commit('cartCounter', cartCounter);
                functions.makeToast(this.$bvToast, 'success', 'Success!', 'Products has been added to cart.', false)
            } else {
                var my_cart = getters.cart
                var cartCounter = getters.cartCounter
                var new_p = product;
                new_p.count = 1;
                delete new_p.description;
                delete new_p.created_at;
                delete new_p.updated_at;
                delete new_p.slug;
                cartCounter = 1;
                my_cart.push(new_p);
                commit('addToCart', my_cart);
                commit('cartCounter', cartCounter);
                functions.makeToast(notify, 'success', 'Success!', 'Product has been added to cart.', false)
            }
        },
        UpdateCartAction({commit, getters}, payload){
            var my_cart = getters.cart
            var cartCounter = getters.cartCounter
            let {product, value, notify, loading } = payload;
            if (Object.keys(my_cart).length) {
                var counter = 0
                for (let i = 0; i < my_cart.length; i++) {
                    if(my_cart[i].id != product.id) {
                        counter = parseInt(counter) + parseInt(my_cart[i].count)
                    }
                }
                cartCounter = parseInt(counter) + parseInt(product.count)
                commit('addToCart', []);
                commit('addToCart', my_cart);
                commit('cartCounter', 0);
                commit('cartCounter', cartCounter);
                functions.makeToast(notify, 'success', 'Success!', 'Cart values updated.', true)
                loading('hide');
            }
        },
        UpdateDiscountsActionBaseOnOldOrders({commit, getters}, payload){
            var my_cart = getters.cart
            var cartCounter = getters.cartCounter
            let {customer_orders} = payload;
            for (let i = 0; i < customer_orders.length; i++) {
                for (let j = 0; j < customer_orders[i].order_details.length; j++) {
                    let obj = _.find(my_cart, function (o) {
                        return o.id === customer_orders[i].order_details[j].id;
                    });
                    if (typeof obj !== 'undefined') {
                        if (obj.buy_1_get_half_off) {
                            var price = obj.price;
                            var ttl_price = price * obj.count
                            var minusTotal = (obj.count/2)
                            minusTotal = Math.floor(minusTotal)
                            obj.sub_total = ttl_price - (price * minusTotal)
                            obj.discount = obj.buy_1_get_half_off_percentage + '% OFF'
                            obj.discount = obj.discount + "<br /> <small><small> " +minusTotal+ ' free product added.</small></small>'
                            var index = _.findIndex(my_cart, {
                                id: obj.id
                            });
                            my_cart.splice(index, 1, obj);
                            commit('addToCart', []);
                            commit('addToCart', my_cart);
                        }
                    }
                }
            }
        },
        UpdateDiscountsActionBaseOnDiscountedProducts({commit, getters}, payload){
            var my_cart = getters.cart
            for (let i = 0; i < my_cart.length; i++) {
                var array = my_cart[i]
                if (array['buy_2_get_1_free']) {
                    var price = array['price']
                    var total_price = price * parseInt(array['count'])
                    var minusTotal = (array['count']/3)
                    minusTotal = Math.floor(minusTotal)
                    array['sub_total'] = total_price - (price * minusTotal)
                    array['discount'] = _.startCase(_.toLower('buy_2_get_1_free)'))
                    array['discount'] = array['discount'] + "<br /> <small><small> " +minusTotal+ ' free product added.</small></small>'
                } else if (!array['discount'] || array['discount'] == 'N/A') {
                    var price = array['price']
                    var total_price = price * parseInt(array['count'])
                    array['discount'] = 'N/A'
                    array['sub_total'] = total_price
                }
                my_cart[i] = array
                commit('addToCart', []);
                commit('addToCart', my_cart);
            }
        },
    }
});
