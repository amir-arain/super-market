var functions = {
  makeToast(obj, variant = 'success', title= 'Success!', text, type = true) {
          obj.toast(text, {
            title: title,
            variant: variant,
            solid: type
          })
      }
}

export default functions