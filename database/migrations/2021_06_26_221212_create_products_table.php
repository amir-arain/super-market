<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('slug');
            $table->text('description');
            $table->unsignedBigInteger('is_available')->default(0);
            $table->double('price', 11, 2);
            $table->string('image');
            $table->boolean('buy_2_get_1_free')->default(true)->comment('This column here is for, to check, if a product has buy_2_get_1_free discount available, while adding into cart, to calculate order amount correctly.');
            $table->boolean('buy_1_get_half_off')->default(true)->comment('This column here is for, to check, if a product has buy_1_get_half_off discount available, while adding into cart, to calculate order amount correctly.');
            $table->unsignedBigInteger('buy_1_get_half_off_percentage')->default(50)->comment('This column here is for, to check, what % of discount a product has if product has buy_1_get_half_off discount applied.');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('products');
    }
}
