<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('order_id');
            $table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('quantity');
            $table->double('sub_total', 15, 2, true);
            $table->boolean('buy_2_get_1_free')->default(true)->comment('This column here is for, to check, if this product had buy_2_get_1_free discount available, when order has been placed, to calculate order amount correctly while showing all orders to Admin/User');
            $table->boolean('buy_1_get_half_off')->default(true)->comment('This column here is for, to check, if this product had buy_1_get_half_off discount available, when order has been placed, to calculate order amount correctly while showing all orders to Admin/User');
            $table->unsignedBigInteger('buy_1_get_half_off_percentage')->default(50)->comment('This column here is for, to check, what % of discount this product had, when order has been placed, if product was buy_1_get_half_off discount applied.');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
