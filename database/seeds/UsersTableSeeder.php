<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $user = new User;
        $user->name = "Admin";
        $user->email = "admin@admin.com";
        $user->password = bcrypt('password');
        $user->is_admin = true;
        $user->email_verified_at = now();
        $user->save();

        $user = new User;
        $user->name = "User";
        $user->email = "user@user.com";
        $user->password = bcrypt('password');
        $user->is_admin = false;
        $user->email_verified_at = now();
        $user->save();
    }
}
