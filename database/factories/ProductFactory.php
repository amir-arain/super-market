<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Product::class, function (Faker $faker) {

    $title = ucfirst($faker->words[0]) . ' ' . ucfirst($faker->words[0]);

    $slug = Str::slug($title, '-');

    if(!is_dir(storage_path('app/public/images'))) {
        mkdir(storage_path('app/public/images'), 0777, true);
    }
    $buy_2_get_1_free = $faker->randomElement([true, false]);
    $buy_1_get_half_off = ($buy_2_get_1_free) ? false : true;
    $buy_1_get_half_off_percentage = ($buy_1_get_half_off) ? 50 : 0;

    return [
        'title' => $title,
        'slug' => $slug,
        'description' => $faker->text(200),
        'is_available' => $faker->randomElement([200, 500, 350, 1000, 20, 10]),
        'price' => rand(2300, 4000),
        'image' => $faker->image(storage_path('app/public/images'), 640, 480, null, false, true),
        'buy_2_get_1_free' => $buy_2_get_1_free,
        'buy_1_get_half_off'=> $buy_1_get_half_off,
        'buy_1_get_half_off_percentage' => $buy_1_get_half_off_percentage,
    ];
});
