<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'title', 'slug', 'description', 'is_available', 'price', 'image', 'buy_2_get_1_free', 'buy_1_get_half_off', 'buy_1_get_half_off_percentage'
    ];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public static function getProductsByIds($ids)
    {
        return self::select('id', 'price', 'buy_2_get_1_free', 'buy_1_get_half_off', 'buy_1_get_half_off_percentage')->whereIn('id', $ids)->get()->toArray();
    }
}
