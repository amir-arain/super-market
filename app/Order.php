<?php
namespace App;

use App\OrderDetails;
use App\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id', 'order_total',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function orderDetails()
    {
        return $this->hasMany(OrderDetails::class);
    }
    public function createOrder($data)
    {
        try {
            $collection = collect($data);
            $ids = $collection->pluck('id')->toArray();
            $data = Product::getProductsByIds($ids);
            foreach ($data as $k => $v) {
                $first = $collection->firstWhere('id', $v['id']);
                $data[$k]['count'] = $first['count'];
            }
            $data = $this->calculate($data);
            if ($data !== false) {
                return $this->insertOrder($data);
            } else {
                return 'some error';
            }
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    private function insertOrder($data)
    {
        try {
            $id = $this->create([
                'user_id' => (auth()->user()) ? auth()->user()->id : null,
                'order_total' => $data['total'],
            ])->id;
            unset($data['total']);
            return $this->insertOrderDetails($data, $id);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    private function insertOrderDetails(array $data, $id): bool
    {
        try {
            $mk_data = [];
            $return = true;
            if (is_array($data) && !empty($data)) {
                foreach ($data as $key => $value) {
                    if (is_array($value) && !empty($value)) {
                        $p_id = $value['id'];
                        $mk_data[$key]['order_id'] = $id;
                        $mk_data[$key]['product_id'] = $p_id;
                        $mk_data[$key]['quantity'] = $value['count'];
                        $mk_data[$key]['sub_total'] = $value['sub_total'];
                        $mk_data[$key]['buy_1_get_half_off'] = $value['buy_1_get_half_off'];
                        $mk_data[$key]['buy_1_get_half_off_percentage'] = $value['buy_1_get_half_off_percentage'];
                        $mk_data[$key]['buy_2_get_1_free'] = $value['buy_2_get_1_free'];
                        $mk_data[$key]['created_at'] = now();
                        $mk_data[$key]['updated_at'] = now();
                    } else {
                        $return = false;
                        break;
                    }
                }
                if($return) {
                    OrderDetails::insert($mk_data);
                    return $return;
                } else {
                    return 'some error';
                }
            }
            return $return;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    private function calculate($data)
    {
        try {
            $return = [];
            $order_total = 0;
            $userOrders = $this->getUserOrders();
            foreach ($data as $k => $v) {
                $return[$k] = $v;
                if ($v['buy_2_get_1_free']) {
                    $total_price = $v['price'] * $v['count'];
                    $minusTotal = floor($v['count'] / 3);
                    $return[$k]['discount'] = true;
                    $return[$k]['sub_total'] = $total_price - ($v['price'] * $minusTotal);
                    $order_total += $return[$k]['sub_total'];
                } else if ($v['buy_1_get_half_off']) {
                    if (in_array($v['id'], $userOrders)) {
                        $price = $v['price'];
                        $ttl_price = $price * $v['count'];
                        $minusTotal = floor($v['count'] / 2);
                        $return[$k]['sub_total'] = $ttl_price - ($price * $minusTotal);
                        $return[$k]['discount'] = true;
                        $order_total += $return[$k]['sub_total'];
                    } else {
                        $total_price = $v['price'] * $v['count'];
                        $return[$k]['discount'] = false;
                        $return[$k]['sub_total'] = $total_price;
                        $order_total += $return[$k]['sub_total'];
                    }
                } else if (!$v['buy_2_get_1_free'] && !isset($v['discount'])) {
                    $total_price = $v['price'] * $v['count'];
                    $return[$k]['discount'] = false;
                    $return[$k]['sub_total'] = $total_price;
                    $order_total += $return[$k]['sub_total'];
                }
            }
            $return['total'] = $order_total;
            return $return;
        } catch (\Throwable $th) {
            return false;
        }
    }
    private function getUserOrders()
    {
        if (!auth()->user()) {
            return [];
        }
        return OrderDetails::whereHas('order', function ($q) {
            $q->where('user_id', auth()->user()->id);
        })->groupBy('product_id')->pluck('product_id')->toArray();
    }
}
