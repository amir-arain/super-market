<?php

namespace App\Traits;

trait ApiResponser
{
    public function successResponse($data, $code = 200, $success = true)
    {
        return response()->json(['success' => $success, 'data' => $data], $code);
    }

    public function errorResponse($data, $code = 500, $success = false)
    {
        return response()->json(['success' => $success, 'data' => $data], $code);
    }
}
