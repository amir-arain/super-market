<?php

namespace App\Imports;

use App\Product;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Faker\Generator as Faker;
use Faker\Factory;

class ProductImport implements ToModel,WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $faker = Factory::create();
        $slug = $this->slugify($row['0']);

        return new Product([
            'title' => $row['0'],
            'slug' => $slug,
            'description' => $faker->text(200), 
            'is_available' => $row['1'],
            'price' => (int) $row['2'],
            'image' => $faker->image(storage_path('app/public/images'), 640, 480, null, false, true),
            'buy_2_get_1_free' => (strtolower($row['3']) == 'on') ? 1 : 0,
            'buy_1_get_half_off' => (strtolower($row['4']) == 'on') ? 1 : 0,
            'buy_1_get_half_off_percentage' => (int) $row['5'],
        ]);
    }
    
    public function startRow(): int
    {
        return 2;
    }

    public function slugify($text, string $divider = '-')
    {
        // replace non letter or digits by divider
        $text = preg_replace('~[^\pL\d]+~u', $divider, $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, $divider);

        // remove duplicate divider
        $text = preg_replace('~-+~', $divider, $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            $text = 'n-a';
        }
        $watch_in_db = Product::where('slug', $text)->count();
        if($watch_in_db > 0) {
            $text = $text . '-' . $watch_in_db;
        }

        return $text;
    }
}
