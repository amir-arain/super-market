<?php

namespace App\Http\Controllers;

use App\Order;
use Auth;
use Illuminate\Http\Request;
use App\Traits\ApiResponser;

class OrderController extends Controller
{
    use ApiResponser;
    public $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function index()
    {
        $orders = [];
        if(auth()->user() && auth()->user()->is_admin) {
            $orders = Order::with(['user', 'orderDetails.product'])->get();
        }
        return $this->successResponse($orders);
    }

    public function store(Request $request)
    {
        $data = json_decode($request->data, true);
        $response = ['error' => 'Something went wrong! Please try again later.'];
        try {
            $return = $this->order->createOrder($data);
            if($return) {
                $response = ['success' => 'Order has been created successfully.'];
                return $this->successResponse($response);
            } else {
                return $this->errorResponse($response, 500);
            }
        } catch (\Throwable $th) {
            return $this->errorResponse($response, 500);
        }
    }

    public function show($id)
    {
    }

    public function update(Request $request, $id)
    {
    }
    
    public function destroy($id)
    {
    }
    
}
