<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Traits\ApiResponser;
use Auth;

class LoginController extends Controller
{
    use ApiResponser;

    public function login(LoginRequest $request)
    {
        if (Auth::attempt($request->only(['email', 'password']))) {
            $data = [
                'user' => Auth::user(),
                'token' => Auth::user()->createToken('token')->accessToken,
            ];
            return $this->successResponse($data);
        }
        $data = ['error' => 'Unauthorized! Wrong email or password.'];
        return $this->errorResponse($data, 401);
    }
}
