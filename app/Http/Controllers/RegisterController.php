<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests\RegisterRequest;
use App\Traits\ApiResponser;
use App\User;

class RegisterController extends Controller
{
    use ApiResponser;

    public function register(RegisterRequest $request)
    {
        try {
            $data = $request->only(['name', 'email', 'password']);
            $data['password'] = bcrypt($data['password']);
            $data['is_admin'] = 0;
            $user = User::create($data);
            Auth::loginUsingId($user->id, true);
            $data = [
                'user' => $user,
                'token' => $user->createToken('token')->accessToken,
            ];
            return $this->successResponse($data);
        } catch (\Exception $e) {
            $data = ['error' => 'Something went wrong! Please try again later.'];
            return $this->errorResponse($data);
        }
    }
}
