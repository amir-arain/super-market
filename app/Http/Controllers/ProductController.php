<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use App\Imports\ProductImport;
use Maatwebsite\Excel\Facades\Excel;

class ProductController extends Controller
{
    use ApiResponser;
    
    public function index()
    {
        $product = Product::latest()->get();
        return $this->successResponse($product);
    }

    public function show($slug)
    {
        try {
            $product = Product::whereSlug($slug)->firstOrFail();
            return $this->successResponse($product);
        } catch (\Throwable $th) {
            $data = ['error' => 'Product not found.'];
            return $this->errorResponse($data);
        }
    }

    public function uploadProduct(Request $request)
    {
        if ($request->hasFile('file')) {
            $newFileName =  time() . '_' . time() . '.' . $request->file('file')->extension();
            $request->file('file')->storeAs('images', $newFileName, 'public');
            Excel::import(new ProductImport, $request->file('file'));
        }
        return $this->successResponse([]);
    }
}
