<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Traits\ApiResponser;

class UserController extends Controller
{
    use ApiResponser;
    public function index()
    {
        $users = [];
        if((auth()->user()) && (auth()->user()->is_admin)) {
            $users = User::with('orders.orderDetails.product')->
            where('is_admin', 0)->
            get();
        }
        return $this->successResponse($users);
    }
}
