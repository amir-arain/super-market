<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Order;
use App\Traits\ApiResponser;

class CustomerController extends Controller
{
    use ApiResponser;

    public function index()
    {
        $orders = Order::with('orderDetails')->where('user_id', auth()->user()->id)->get();
        return $this->successResponse($orders);
    }
    public function ordersWithProducts()
    {
        $orders = Order::latest()->with('orderDetails.product')->where('user_id', auth()->user()->id)->get();
        return $this->successResponse($orders);
    }

}
