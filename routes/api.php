<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::post('login', 'LoginController@login');
Route::post('logout', function (){
    \Auth::logout();
});
Route::post('register', 'RegisterController@register');
Route::get('/products', 'ProductController@index');
Route::get('/product/{product}', 'ProductController@show');

Route::group(['middleware' => 'auth:api'], function(){
    Route::get('/users','UserController@index');
    Route::resource('order', 'OrderController');
    Route::get('customer/orders', 'CustomerController@index');
    Route::get('customer/orders-with-products', 'CustomerController@ordersWithProducts');
    Route::post('upload-products', 'ProductController@uploadProduct');
});
